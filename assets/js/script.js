$('.videofrmDv').slick({
    infinite: true,
    speed: 500,
    cssEase: 'linear',
    arrows:false,
    slidesToShow: 1,
    dots:false
});
$('.sliderDv .pagersDv figure').on('click',function(){
    if($(this).hasClass('pagerDvnext')){
        $('.videofrmDv').slick('slickNext');
    }
    if($(this).hasClass('pagerDvprev')){
        $('.videofrmDv').slick('slickPrev');
    }
});
